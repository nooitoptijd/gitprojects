package com.example.mustafakuzu.amazighstorybook;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mustafakuzu.amazighstorybook.database.DatabaseHelper;


public class SpreekwoordenActivity extends ActionBarActivity {

    LayoutInflater layoutInflater;
    int Categoryid;

    TextView tvSpreekwoord;
    TextView tvVertaling;
    TextView tvBetekenis;

    String data[][];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spreekwoorden);

        layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Bundle extras = getIntent().getExtras();
        Categoryid = extras.getInt("Catagory");

        //data ophalen
        DatabaseHelper db = new DatabaseHelper(this);
        // hier staat alle data in een array
        data = db.getdata2(Categoryid);
        for (int i=0;data.length > i;i++){

            showEntry(data[i][0], data[i][1], data[i][2], data[i][3]);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_spreekwoorden, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showEntry(String id, String spreekwoord, String vertaling, String betekenis) {

        //add to view
        View newSRow = layoutInflater.inflate(R.layout.spreekwoordrow, null);

        //set tekst of textview
       /* tvSpreekwoord = (TextView) newSRow.findViewById(R.id.Spreekwoord);
        tvSpreekwoord.setText(spreekwoord);
        tvVertaling = (TextView) newSRow.findViewById(R.id.Translation);
        tvVertaling.setText(vertaling);
        tvBetekenis = (TextView) newSRow.findViewById(R.id.Meaning);
        tvBetekenis.setText(betekenis);*/

        //set tekst of button
        Button bSpreekwoord = (Button) newSRow.findViewById(R.id.button2);
        bSpreekwoord.setText(spreekwoord);
        bSpreekwoord.setId(Integer.parseInt(id));


        //set where the content gets added
        TableLayout tagsTableLayout = (TableLayout) findViewById(R.id.querySpreekTableLayout);
        tagsTableLayout.addView(newSRow);
    }

    public void Spreekwoord (View view) {
        //declare followup activity
        Intent intent = new Intent(SpreekwoordenActivity.this, SpreekwoordActivity.class);

        Button a = (Button) findViewById(view.getId());
        String b = a.getText().toString();

        int c = 101;

        //give data to next activity
        intent.putExtra("Spreekwoord", b);
        for (int i=0;data.length > i;i++) {
            if (data[i][1] == b) {
                c = Integer.parseInt(data[i][0]);
            }
        }
        //start followup activity
        intent.putExtra("ID", c);
        startActivity(intent);
    }
}
