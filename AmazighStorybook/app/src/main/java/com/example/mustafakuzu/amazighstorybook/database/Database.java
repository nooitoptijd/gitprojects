package com.example.mustafakuzu.amazighstorybook.database;

import android.provider.BaseColumns;

public final class Database {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public Database() {}

    /* Inner class that defines the table contents */
    public static abstract class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "Proverbs";
        public static final String COLUMN_NAME_ID = "entryid";
        public static final String[] Proverbs = new String[]{"Proverbs","id","CatagoryId","Amazig","Dutch","Meaning","Sound","fav"};
        public static final String[] Catagorys = new String[]{"Catagorys","CatagorysId","CatagorysName"};
        //public static final String COLUMN_NAME_TITLE = "title";
        public static final String SQL_CREATE_TBL1 =
                "CREATE TABLE " + FeedEntry.Proverbs[0] + " (" +
                        FeedEntry.Proverbs[1] + " INTEGER PRIMARY KEY," +
                        FeedEntry.Proverbs[2] + ","+
                        FeedEntry.Proverbs[3] + ","+
                        FeedEntry.Proverbs[4] + ","+
                        FeedEntry.Proverbs[5] + ","+
                        FeedEntry.Proverbs[6] + ","+
                        FeedEntry.Proverbs[7] +
                " )";
        public static final String SQL_CREATE_TBL2 =
                "CREATE TABLE " + FeedEntry.Catagorys[0] + " (" +
                        FeedEntry.Catagorys[1] + " INTEGER PRIMARY KEY," +
                        FeedEntry.Catagorys[2] +
                " );";
        //public  static final String SQL_CREATE_ENTRIES =
        //        "CREATE TABLE Proverbs ID INTEGER PRIMARY KEY " +
        //            "Amazig " +
        //            "Dutch " +
        //            "Meaning " +
        //            "CatagoryId; " +
        //        "CREATE TABLE Catagorys ID INTEGER PRIMARY KEY " +
        //             "CatagoryId " +
        //             "CatagoryName; ";
        public static final String SQL_DELETE_ENTRIES1 =
                "DROP TABLE IF EXISTS "+Proverbs[0] +";";
        public static final String SQL_DELETE_ENTRIES2 =
                "DROP TABLE IF EXISTS "+Catagorys[0] +";";
        public static String COLUMN_NAME_NULLABLE;
    }

}
