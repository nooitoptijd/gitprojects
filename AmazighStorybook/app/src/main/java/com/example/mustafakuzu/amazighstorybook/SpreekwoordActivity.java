package com.example.mustafakuzu.amazighstorybook;

import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mustafakuzu.amazighstorybook.database.DatabaseHelper;


public class SpreekwoordActivity extends ActionBarActivity {

    int ID;

    TextView Proverb;
    TextView meaning;
    TextView Translation;
    DatabaseHelper database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spreekwoord);

        Bundle extras = getIntent().getExtras();
        ID = extras.getInt("ID");
        database = new DatabaseHelper(this);
        //0=Amazig 1=Dutch 2=Meaning 3=Sound
        String data[] = database.getproverb(ID);
        showText(data);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_spreekwoord, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showText(String data[]){
        //get textview's
        Proverb = (TextView) findViewById(R.id.Spreekwoord);
        meaning = (TextView) findViewById(R.id.uitleg);
        Translation = (TextView) findViewById(R.id.vertaling);

        //set text of the textview's
        Proverb.setText(data[0]);
        meaning.setText(data[2]);
        Translation.setText(data[1]);
    }

    //button functies
    //geluid button
    public void sound(View view){

    }

    //favorieten button
    public void favoriet(View view){
        database.favorites(ID);
    }
}
