package com.example.mustafakuzu.amazighstorybook;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.mustafakuzu.amazighstorybook.database.Database;
import com.example.mustafakuzu.amazighstorybook.database.DatabaseHelper;

import java.io.IOException;
import java.security.AccessControlContext;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DatabaseHelper db = new DatabaseHelper(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //start new activity
    private class CategoryRunnable implements Runnable{
        @Override
        public void run() {
            Intent i = new Intent(MainActivity.this, CategoryActivity.class);
            startActivity(i);
        }
    }
    private class OptionsRunnable implements Runnable{
        @Override
        public void run() {
            Intent i = new Intent(MainActivity.this, OptionsActivity.class);
            startActivity(i);
        }
    }
   /* private class ThirdRunnable implements Runnable{
        @Override
        public void run() {
            Intent i = new Intent(MainActivity.this, ThirdActivity1.class);
            startActivity(i);
        }
    }*/

    //buttons
    public void category(View view){//button1
        CategoryRunnable i = new CategoryRunnable();
        i.run();

        // remove toast when button is used
        // toast();
    }

    public void OptionsActivity(View view){//button2
        OptionsRunnable i = new OptionsRunnable();
        i.run();
    }

    public void exit(View view){
        MainActivity.this.finish();
        System.exit(0);
    }
    public void toast(){
        final Toast toast = Toast.makeText(getApplicationContext(), "placeholder button", Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 500);
    }
}


