package com.example.mustafakuzu.amazighstorybook.database;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View;
import android.widget.Toast;

import com.example.mustafakuzu.amazighstorybook.MainActivity;
import com.example.mustafakuzu.amazighstorybook.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.AccessControlContext;

public class DatabaseHelper extends SQLiteOpenHelper {
    // If you change the test schema, you must increment the test version.
    public static final int DATABASE_VERSION = 36;
    public static final String DATABASE_NAME = "Database.db";
    private Activity context;
    public DatabaseHelper(Activity context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public void onCreate(SQLiteDatabase db) {
        System.out.println(Database.FeedEntry.SQL_CREATE_TBL1);
        System.out.println(Database.FeedEntry.SQL_CREATE_TBL2);
        db.execSQL(Database.FeedEntry.SQL_CREATE_TBL1);
        db.execSQL(Database.FeedEntry.SQL_CREATE_TBL2);
        try {
            insertFromFile(db, R.raw.test);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This test is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(Database.FeedEntry.SQL_DELETE_ENTRIES1);
        db.execSQL(Database.FeedEntry.SQL_DELETE_ENTRIES2);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    /**
     * This reads a file from the given Resource-Id and calls every line of it as a SQL-Statement
     *
     *
     * @param resourceId
     *  e.g. R.raw.food_db
     *
     * @return Number of SQL-Statements run
     * @throws IOException
     */
    public int insertFromFile(SQLiteDatabase db, int resourceId) throws IOException {
        // Reseting Counter
        int result = 0;
        //SQLiteDatabase db = this.getReadableDatabase();
        // Open the resource
        InputStream insertsStream = context.getResources().openRawResource(resourceId);
        BufferedReader insertReader = new BufferedReader(new InputStreamReader(insertsStream));

        // Iterate through lines (assuming each insert has its own line and theres no other stuff)
        while (insertReader.ready()) {
            String insertStmt = insertReader.readLine();
            db.execSQL(insertStmt);
            result++;
        }
        insertReader.close();

        // returning number of inserted rows
        return result;
    }

    public String[][] getCatagoryAndId(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("Select " + Database.FeedEntry.Catagorys[1]+","+Database.FeedEntry.Catagorys[2]+" from "+Database.FeedEntry.Catagorys[0]+" ORDER BY "+Database.FeedEntry.Catagorys[1],null);
        String[][] output = new String[c.getCount()][2];
        int i = 0;
        while (c.moveToNext()) {
            output[i][0] = c.getString(c.getColumnIndex(Database.FeedEntry.Catagorys[1]));
            output[i][1] = c.getString(c.getColumnIndex(Database.FeedEntry.Catagorys[2]));
            i++;
        }
        return output;
    }
    public String[][] getdata2(int catagory) {
        String where;
        if (catagory == 100){
             where = " where "+ Database.FeedEntry.Proverbs[7]+"=1";
        } else{
             where = " where "+ Database.FeedEntry.Catagorys[1]+"="+catagory;
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(
                "SELECT * from " + Database.FeedEntry.Proverbs[0] +
                        " join " + Database.FeedEntry.Catagorys[0] +
                        " on " + Database.FeedEntry.Proverbs[0] + "." + Database.FeedEntry.Proverbs[2] +
                        " = " + Database.FeedEntry.Catagorys[0] + "." + Database.FeedEntry.Catagorys[1] + where
                , null);
        String[][] output = new String[c.getCount()][6];
        int i = 0;
        while (c.moveToNext()) {
            output[i][0] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[1]));
            output[i][1] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[3]));
            output[i][2] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[4]));
            output[i][3] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[5]));
            output[i][4] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[6]));
            output[i][5] = c.getString(c.getColumnIndex(Database.FeedEntry.Catagorys[2]));
            i++;
        }
        return output;
    }
    public String[][] getdata() {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(
                "SELECT * from " + Database.FeedEntry.Proverbs[0] +
                        " join " + Database.FeedEntry.Catagorys[0] +
                        " on " + Database.FeedEntry.Proverbs[0] + "." + Database.FeedEntry.Proverbs[2] +
                        " = " + Database.FeedEntry.Catagorys[0] + "." + Database.FeedEntry.Catagorys[1], null);
        String[][] output = new String[c.getCount()][7];
        int i = 0;
        while (c.moveToNext()) {
            output[i][0] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[1]));
            output[i][1] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[3]));
            output[i][2] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[4]));
            output[i][3] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[5]));
            output[i][4] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[6]));
            output[i][5] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[7]));
            output[i][6] = c.getString(c.getColumnIndex(Database.FeedEntry.Catagorys[2]));
            i++;

        }
        return output;
    }
    public void favorites(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * from " + Database.FeedEntry.Proverbs[0]+" where "+Database.FeedEntry.Proverbs[1]+" = "+id+";" , null);
        c.moveToFirst();
        int i = c.getInt(c.getColumnIndex(Database.FeedEntry.Proverbs[7]));
        if (i == 0){
            db.execSQL("UPDATE Proverbs SET fav = 1 WHERE id ="+id);
        }else {
            db.execSQL("UPDATE Proverbs SET fav = 0 WHERE id ="+id);
        }
    }
    public void addFav (int id){
        SQLiteDatabase db = this.getReadableDatabase();
        db.rawQuery("INSERT INTO "+Database.FeedEntry.Proverbs[0]+" ("+Database.FeedEntry.Proverbs[7]+") VALUES (1) where "+Database.FeedEntry.Proverbs[1]+"="+id+";",null);
    }
    public void removeFav (int id){
        SQLiteDatabase db = this.getReadableDatabase();
        db.rawQuery("INSERT INTO "+Database.FeedEntry.Proverbs[0]+" ("+Database.FeedEntry.Proverbs[7]+") VALUES (0) where "+Database.FeedEntry.Proverbs[1]+"="+id+";",null);
    }
    public String[] getproverb(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(
                "SELECT "+Database.FeedEntry.Proverbs[3]+","+Database.FeedEntry.Proverbs[4]+","+Database.FeedEntry.Proverbs[5]+","+Database.FeedEntry.Proverbs[6]+
                        " from " + Database.FeedEntry.Proverbs[0]+
                        " where "+ Database.FeedEntry.Proverbs[1]+"="+id
        ,null);
        int i = 0;
        String[] output = new String[4];
        c.moveToFirst();
            output[0] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[3]));
            output[1] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[4]));
            output[2] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[5]));
            output[3] = c.getString(c.getColumnIndex(Database.FeedEntry.Proverbs[6]));
        return output;
    }
}