package com.example.mustafakuzu.amazighstorybook;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.Toast;

import com.example.mustafakuzu.amazighstorybook.database.DatabaseHelper;


public class CategoryActivity extends ActionBarActivity {

    LayoutInflater layoutInflater;
    DatabaseHelper mDbHelper;
    String[][] catagory;
    String[] catagoryid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mDbHelper = new DatabaseHelper(this);
        String[][] catagory = mDbHelper.getCatagoryAndId();

        for (int i=0;catagory.length > i;i++){
            showEntry(catagory[i][1], catagory[i][0]);
        }

        //button for favorites
        showEntry("Favorieten", "100");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showEntry(String naam, String idstring) {
        //add to view
        View newRow = layoutInflater.inflate(R.layout.categoryrow, null);

        //set tekst of button
        Button button = (Button) newRow.findViewById(R.id.category);
        button.setText(naam);

        //set id of button
        int id = Integer.parseInt(idstring);
        button.setId(id);

        //set where the content gets added
        TableLayout tagsTableLayout = (TableLayout) findViewById(R.id.queryTableLayout);
        tagsTableLayout.addView(newRow);
    }

    public void Category (View view) {
        //declare followup activity
        Intent intent = new Intent(CategoryActivity.this, SpreekwoordenActivity.class);

        //give category to next activity
        Button a = (Button) findViewById(view.getId());

        int catagory = a.getId();

        intent.putExtra("Catagory", catagory);

        //start followup activity
        startActivity(intent);
    }
}
